#-------------------------------------------------
#
# Project created by QtCreator 2016-05-26T13:49:06
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KirkayakTest
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    Kirkayak.cpp \
    listserial.cpp

HEADERS += mainwindow.h \
    Kirkayak.hpp

FORMS += mainwindow.ui

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libmodbus

win32: LIBS += -L$$PWD/libmodbus/ -lmodbus
win32: INCLUDEPATH += $$PWD/libmodbus
win32: DEPENDPATH += $$PWD/libmodbus
win32: SOURCES += enumser.cpp
win32: HEADERS += enumser.h \
    stdAfx.h
