#include "Kirkayak.hpp"

Kirkayak::Kirkayak(std::string port)
    : maxSpeed(250)
{
    ctx = modbus_new_rtu(port.c_str(), 115200, 'N', 8, 1);
    if (ctx == NULL) {
        std::cerr << "Unable to access port!";
    }

    modbus_set_slave(ctx, 1);
    modbus_connect(ctx);
}

Kirkayak::~Kirkayak()
{
    modbus_close(ctx);
    modbus_free(ctx);
}

void Kirkayak::setSpeed(uint16_t sps)
{
    maxSpeed = sps; // Steps per second
    // TODO: If it's moving, more needs to be done.
}

void Kirkayak::moveAbs(int32_t x, int32_t y, bool wait)
{
    uint16_t buffer[6];
    lastX = x;
    lastY = y;

    // Calculating the axis speeds
    int32_t xNow, yNow;
    uint16_t xSpd = maxSpeed, ySpd = maxSpeed;
    getPosition(&xNow, &yNow);
    float xDiff = (float)abs(x - xNow);
    float yDiff = (float)abs(y - yNow);
    if (xDiff > yDiff) ySpd = (int16_t)(maxSpeed * (yDiff / xDiff));
    else if (yDiff > xDiff) xSpd = (int16_t)(maxSpeed * (xDiff / yDiff));

    *((int32_t*)buffer) = x; // Holding 5 & 6
    *((int32_t*)(buffer + 2)) = y; // Holding 7 & 8
    buffer[4] = speedToPeriod(xSpd);
    buffer[5] = speedToPeriod(ySpd);
    modbus_write_registers(ctx, 5, 6, buffer);

    if (wait) while(isMoving()) sleepMs(100);
}

void Kirkayak::moveRel(int32_t x, int32_t y, bool wait)
{
    int32_t xNow, yNow;
    getPosition(&xNow, &yNow);
    moveAbs(xNow + x, yNow + y, wait);
}

void Kirkayak::stop()
{
    moveRel(0, 0);
}

bool Kirkayak::isMoving()
{
    uint16_t buffer[8];
    int32_t *buffer32 = (int32_t*)buffer;
    modbus_read_registers(ctx, 1, 8, buffer);
    return (buffer32[0] != buffer32[2] || buffer32[1] != buffer32[3]);
}

void Kirkayak::homeAll(bool wait)
{
    moveAbs(INT32_MIN/2, INT32_MIN/2, wait);
}

void Kirkayak::getPosition(int32_t* x, int32_t* y)
{
    uint16_t buffer[4];
    int32_t *buffer32 = (int32_t*)buffer;
    modbus_read_registers(ctx, 1, 4, buffer);
    *x = buffer32[0];
    *y = buffer32[1];
}

uint16_t Kirkayak::speedToPeriod(uint16_t sps)
{
    int period;
    if (sps == 0) sps = 1; // Dirty fix for division by zero
    period = 250000 / sps;
    if (period > 32000) return 32000;
    else if (period < 100) return 100;
    else return period;
}

void Kirkayak::sleepMs(int ms)
{
#ifdef __linux__
    usleep(ms * 1000);
#endif
#ifdef _WIN32
    Sleep(ms);
#endif
}
