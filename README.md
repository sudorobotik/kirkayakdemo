Kirkayak Demo
=============

**Türkçe için sayfanın altına bakınız.**

Overview
--------
KirkayakDemo is an example application with a simple GUI for the Kirkayak -  a
commertial 2 axis stepper motor driver board from Sudo Robotik. Kirkayak is
designed to be controlled via Modbus protocol.

This demo application consists of two layers:

1. A C++ library (Kirkayak.hpp/cpp) to wrap modbus access functions provided by
*libmodbus*. You can use this library in your C++ application.
2. An example Qt project written in C++ on top of this library.

Kirkayak.hpp provides basic functionality to control a 2 axis table. You can set
move speed, order it to move a specific position or return to home position. It
can also poll the current position even if the table is still moving.

Installation
------------
On GNU/Linux it's quite easy. You need libmodbus. Probably you will be able to
install it using your package manager. If a package is not available for your
distro, you need to compile it yourself but I guess it's straight forward. You
may need special permission to access the serial port. On Ubuntu, you simply add
your user to dialout group. When you're done, simply open the project using
Qt Creator.

Installation on Windows is hard, mostly because of libmodbus. Let me list the
steps:

1. Download the lastest libmodbus from [github](https://github.com/stephane/libmodbus).
At the time of writing, the downloadable archive on their website is missing
some required files.
2. Download and install Visual Studio with C++ support.
3. You also need [MinGW](http://www.mingw.org/). As the libmodbus README suggests:
>  To compile under Windows, install MinGW and MSYS then select the common
>  packages (gcc, automake, libtool, etc). The directory ./src/win32/ contains a
>  Visual C project.

4. You need to run *configure.js* in the ./src/win32/ folder. If it doesn't work,
run `cscript configure.js` in command line instead of double clicking it.
5. Open the modbus-9.sln in the src/win32 folder. Compile the project. If it
complains about something like VERSION:1.0.0, go to Project Properties > Linker
and delete 1.0.0 from Version row. After compiling you will have modbus.lib and
modbus.dll in win32 folder.
6. Copy modbus.lib and all the header files (*.h) located under src into the
libmodbus folder under this KirkayakDemo project.
7. Of course, you also need Qt Framework but you need the 32 bit one. Be sure to
select it during the installation. If you have already installed Qt Creator but
don't have the 32 bit support, you can add it using Add/Remove Programs under
Control Panel.
8. Now you should be able to compile the project. Qt Creator creates another
folder for the binary output outside the project folder and this is where the
KirkayakDemo.exe is located. To be able to run it, you need the modbus.dll
placed in folder where the .exe file is located.

If all you need is just the binary, you can download it from the downloads section.

Dependencies & Thanks
---------------------
As it's obvious, this project uses [libmodbus](http://libmodbus.org/). Also, for
serial port discovery, I use some external code. For Windows, I use
[EnumSerial](http://www.naughter.com/enumser.html). For GNU/Linux, I use a code
segment given by *Søren Holm* in [stackoverflow](http://stackoverflow.com/questions/2530096/how-to-find-all-serial-devices-ttys-ttyusb-on-linux-without-opening-them).
I thank these authors.

Contact
-------
Gökçe Tağlıoğlu - tagli@sudorobotik.com.tr

---

Genel Bakış
-----------
KirkayakDemo, Sudo Robotik'in ürettiği ticari bir 2 eksenli step motor kontrol
kartı olan Kirkayak için yazılmış grafik arayüzlü bir örnek uygulamadır. Kirkayak,
Modbus protokolü üzerinden kontrol edilmek üzere tasarlanmıştır.

Bu demo uygulaması iki katmandan oluşur:

1. *libmodbus* kütüphanesi üzerine yazılmış bir C++ kütüphanesi (Kirkayak.hpp/cpp).
Bu kütüphaneyi kendi C++ uygulamanızda kullanabilirsiniz.
2. Söz konusu kütüphaneyi kullanan C++'ta yazılmış örnek bir Qt projesi.

Kirkayak.hpp 2 eksenli bir tablayı kontrol etmek için gereken temel fonksiyonları
sunar. Bunlardan bazıları, hareket hızı ayarlama, belli bir noktaya gitme veya
başlangıç noktasına dönerek sıfırlama yapmaktır. Ayrıca, tabla hareket halinde
olması durumu da dahil olmak üzere herhangi bir anda tabla konumu sorgulanabilir.

Kurulum
-------
Kurulum işlemi GNU/Linux sistemlerde oldukça kolaydır. Öncelikle libmodbus
kütüphanesini yüklemeniz gerekiyor. Bunu muhtemelen GNU/Linux dağıtımınızın paket
yöneticisini kullanarak rahatlıkla yapabilirsiniz. Eğer dağıtımınız için hazır
bir paket yoksa, kütüphanenin kaynak kodunu çekip kendiniz derlemeniz gerekebilir
ancak bu işlem de pek zahmet gerektirmeyen bir işlemdir. Seri porta erişebilmek
için özel sistem izinine ihtiyacınız olabilir. Örneğin Ubuntu'da, kullanıcınızı
dialout grubuna eklemeniz gerekmektedir. Bu işlemleri tamamladığınızda, projeyi
Qt Creator ile açıp derleyebilirsiniz.

Maalesef Window ortamında kurulum zor ve karmaşık bir işlem. Bu zorluğun temel
sebebi libmodbus kurulumunun zor olması. Aşağıdaki adımları izlemeniz gerekiyor:

1. libmodbus'ın son versiyonunu [github](https://github.com/stephane/libmodbus)
hesabı üzerinden indirin. Bu yazının yazıldığı sırada, normal websitesinden
indirilen arşiv dosyası gerekli bazı dosyaları içermediği için kullanılamıyordu. 
2. Visual Studio'yu bilgisayarınıza kurun. Kurulum sırasında C++ desteğini de
seçtiğinizden emin olun.
3. [MinGW](http://www.mingw.org/)'yi bilgisayarınıza kurun. Kurulumda mingw32-base,
mingw32-gcc-g++ ve msys-base bileşenlerini seçip kurduğunuzdan emin olun.
4. İndirdiğiniz libmodbus kodu içinde src/win32 klasörü altında bulunan
*configure.js* dosyasını çift tıklayarak çalıştırın. Eğer çalışmazsa, komut
satırında `cscript configure.js` komutu ile çalıştırabilirsiniz.
5. src/win32 klasörü altında bulunan modbus-9.sln dosyasını açın ve projeyi
derleyin. Eğer içinde VERSION:1.0.0 gibi bir metin geçen bir hata mesajı ile
karşılaşırsanız, Project Properties altında Linker ayarlarına giderek Version
karşısında bulunan 1.0.0 yazısını siliniz. Derlemenin tamamlanmasının ardından
win32 klasörü altında modbus.lib ve modbus.dll dosyaları oluşacaktır.
6. modbus.lib and src altında bulunan tüm header dosyalarını (*.h) KirkayakDemo
projesi altında bulunan libmodbus klasörü altına kopyalayın.
7. Qt Creator'ı bilgisayarınıza kurmanız gerekiyor ancak 32 bitlik framework'e
ihtiyacınız var. Kurulum sırasında bunun seçili olduğundan emin olun (varsayılan
ayarlarda seçili değildir). Eğer Qt'u 32 bit desteği olmadan kurduysanız, Denetim
Masası'nda Program Ekle/Kaldır ile kurulum ekranını tekrar açarak ilgili bileşeni
seçip yüklenmesini sağlayabilirsiniz.
8. Bu noktada projeyi derleyebiliyor olmanız gerekiyor. Qt Creator derleme
işleminin ardından proje klasörünün dışında yeni bir klasör oluşturacak ve 
Kirkayak.exe dosyasını bunun altındaki debug klasörüne koyacak. Programı
çalıştırabilmek için, modbus.dll dosyasının .exe dosyası ile aynı yerde olması
gerekiyor.

Eğer programı sadece çalıştırılabilir hali ile ilgileniyorsanız doğrudan Downloads
bölümünden indirebilirsiniz.

Bağımlılıklar ve Teşekkür
-------------------------
Malum olduğu üzere, bu projede [libmodbus](http://libmodbus.org/) kullanılmıştır.
Ayrıca, seri portların tespit edilmesi için gereken bazı kodlar dışarıdan alınmıştır.
Windows için, [EnumSerial](http://www.naughter.com/enumser.html), GNU/Linux için
ise *Søren Holm* tarafından [stackoverflow](http://stackoverflow.com/questions/2530096/how-to-find-all-serial-devices-ttys-ttyusb-on-linux-without-opening-them)'da
verilen kod parçası kullanılmıştır. İlgili yazarlara teşekkürlerimi sunarım.

İletişim
--------
Gökçe Tağlıoğlu - tagli@sudorobotik.com.tr

