#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <iostream>
#include <list>
#include "Kirkayak.hpp"

using namespace std;

// External functions
list<string> getComList();

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void kirkayakPoller();
    void kirkayakMove();
    void portOpenClose();

private:
    bool portOpen;
    Kirkayak *stage;
    QTimer *poller;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
