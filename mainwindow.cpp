#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), portOpen(false), stage(NULL),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Creating the list of available serial ports
    list<string> tempList = getComList();
    list<string>::iterator it = tempList.begin();
    QStringList comList;
    while (it != tempList.end()) {
        QString tempStr = it->c_str();
        comList << tempStr;
        it++;
    }
    ui->portCombo->addItems(comList);

    // Setting up the timer object for position polling
    poller = new QTimer(this);
    connect(poller, SIGNAL(timeout()), this, SLOT(kirkayakPoller()));
    poller->start(200);

    // Connecting buttons to slots
    connect(ui->homeButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->upButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->downButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->leftButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->rightButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->goButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(kirkayakMove()));
    connect(ui->portButton, SIGNAL(clicked()), this, SLOT(portOpenClose()));

}

MainWindow::~MainWindow()
{
    delete stage;
    delete ui;
}

void MainWindow::kirkayakPoller()
{
    if (portOpen) {
        int32_t xPos = 0, yPos = 0;
        stage->getPosition(&xPos, &yPos);
        ui->xLcd->display(xPos);
        ui->yLcd->display(yPos);
    }
}

void MainWindow::kirkayakMove()
{
    stage->setSpeed(ui->speedSpin->value());
    int16_t step = ui->stepSpin->value();
    QPushButton *button = (QPushButton*)sender();
    if (button == ui->homeButton) stage->homeAll();
    else if (button == ui->upButton) stage->moveRel(0, step, false);
    else if (button == ui->downButton) stage->moveRel(0, -step, false);
    else if (button == ui->leftButton) stage->moveRel(step, 0, false);
    else if (button == ui->rightButton) stage->moveRel(-step, 0, false);
    else if (button == ui->goButton) stage->moveAbs(ui->xTargetSpin->value(), ui->yTargetSpin->value());
    else if (button == ui->stopButton) stage->stop();
}

void MainWindow::portOpenClose()
{
    if (!portOpen) {
        stage = new Kirkayak(ui->portCombo->currentText().toStdString());
        ui->controlGroup->setEnabled(true);
        ui->portButton->setText("Close");
        ui->portCombo->setEnabled(false);
        portOpen = true;
    }
    else {
        delete stage;
        ui->controlGroup->setEnabled(false);
        ui->portButton->setText("Open");
        ui->portCombo->setEnabled(true);
        portOpen = false;
    }
}
