#ifndef KIRKAYAK_HPP
#define KIRKAYAK_HPP

#include <iostream>
#include "modbus.h"
#include <string>
#include <stdint.h>
#include <stdlib.h>

#ifdef __linux__
#include <unistd.h>
#endif
#ifdef _WIN32
#include <windows.h>
#endif

class Kirkayak
{
public:
	Kirkayak(std::string port = "/dev/ttyUSB0");
	~Kirkayak();
	
	void setSpeed(uint16_t tps);
    void moveAbs(int32_t x, int32_t y, bool wait = false);
    void moveRel(int32_t x, int32_t y, bool wait = false);
	void stop();
	bool isMoving();
	void homeAll(bool wait = false);
    void getPosition(int32_t* x, int32_t* y);
	
private:
	modbus_t *ctx;
	uint16_t maxSpeed;
    int32_t lastX, lastY;
	
	uint16_t speedToPeriod(uint16_t sps);
    void sleepMs(int ms);
	
};
#endif
